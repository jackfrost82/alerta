import logging
import os
import requests
import json

try:
    from alerta.plugins import app  # alerta >= 5.0
except ImportError:
    from alerta.app import app  # alerta < 5.0
from alerta.plugins import PluginBase

LOG = logging.getLogger('alerta.plugins.jsonify')

JSONIFY_WEBHOOK_URL = os.environ.get('JSONIFY_WEBHOOK_URL') or app.config['JSONIFY_WEBHOOK_URL']


class PostMessage(PluginBase):

    def pre_receive(self, alert):
        return alert

    def post_receive(self, alert):
        LOG.info('Alert post receive enter')
        printedAlert = str(alert)
        LOG.info("alert iniziale %s" % printedAlert)
        id = alert.id
        r = requests.request("POST", JSONIFY_WEBHOOK_URL, data=id )
        
    def status_change(self, alert, status, text):
        return alert
