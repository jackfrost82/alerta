
from setuptools import setup, find_packages

version = '5.0.0'

setup(
    name="alerta-jsonify",
    version=version,
    description='Alerta send alert in json format to url',
    url='https://jackfrost82@bitbucket.org/jackfrost82/alerta.git',
    license='MIT',
    author='Beggi',
    author_email='jackfrost82@gmail.com',
    packages=find_packages(),
    py_modules=['alerta_jsonify'],
    install_requires=[
        'requests'
    ],
    include_package_data=True,
    zip_safe=True,
    entry_points={
        'alerta.plugins': [
            'jsonify = alerta_jsonify:PostMessage'
        ]
    }
)
