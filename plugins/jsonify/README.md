Alerta to json Plugin
==================

Send alert to url in json format

Installation
------------

Clone the GitHub repo and run:

    $ python setup.py install

Or, to install remotely from GitHub run:

    $ pip install git+https://github.com/alerta/alerta-contrib.git#subdirectory=plugins/rocketchat

Note: If Alerta is installed in a python virtual environment then plugins
need to be installed into the same environment for Alerta to dynamically
discover them.

Configuration
-------------

PLUGINS = ['jsonify']
JSON_WEBHOOK_URL = [array of url]
