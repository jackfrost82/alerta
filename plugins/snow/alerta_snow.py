import logging
import os
import requests
import json

try:
    from alerta.plugins import app  # alerta >= 5.0
except ImportError:
    from alerta.app import app  # alerta < 5.0
from alerta.plugins import PluginBase

LOG = logging.getLogger('alerta.plugins.snow')
#logging.basicConfig(level=logging.DEBUG)

SNOW_WEBHOOK_URL = os.environ.get('SNOW_WEBHOOK_URL') or app.config['SNOW_WEBHOOK_URL']
SNOW_AUTH = os.environ.get('SNOW_AUTH') or app.config['SNOW_AUTH']
COMPANY = os.environ.get('COMPANY') or app.config['COMPANY']
CATEGORIZATION = os.environ.get('CATEGORIZATION') or app.config['CATEGORIZATION']
headers = { 'Authorization': "Basic %s" % SNOW_AUTH }

class PostMessage(PluginBase):

    def pre_receive(self, alert):
        return alert

    def post_receive(self, alert):
        LOG.info('Alert post receive enter')
        printedAlert = str(alert)
        LOG.info("alert iniziale %s" % printedAlert)
        ServiceNowID = alert.attributes.get('ServiceNow', None)

        if alert.repeat:
            LOG.info('Alert post receive repeat %s: %s' % (alert.id, alert.get_body(history=False)))
            return alert
        
        checkSnow = "noIncident" 
        try:
            checkSnow = alert.attributes['ServiceNow']
        except KeyError:
            pass

        if checkSnow == "noIncident":
            if alert.severity == 'critical':
                LOG.info('Alert post receive critical %s: %s' % (alert.id, alert.get_body(history=False)))
                payload = {
                	"ServiceNow": {
                		"ticket_type": "incident",
                		"short_description": "%s: %s alert for %s - %s is %s" % (alert.environment, alert.severity.capitalize(),','.join(alert.service), alert.resource, alert.event),
                		"description":  "%s: %s alert for %s - %s is %s" % (alert.environment, alert.severity.capitalize(),','.join(alert.service), alert.resource, alert.event),
                		"company": "%s" % COMPANY,
                		"state": "Active",
                		"impact": "4",
                		"urgency": "4",
                		"location": "UNKNOWN",
                		"u_categorization": "%s" % CATEGORIZATION,
                		"contact_type": "Auto-Generated Event"
                	}
                }        
                LOG.info('Snow: %s', payload)

                try:
                    response = requests.request("POST", SNOW_WEBHOOK_URL, json=payload, headers=headers)
                    data = json.loads(response.text)
                    alert.attributes['ServiceNowStatus'] = "active"
                    alert.attributes['ServiceNow'] = data["ID"]
                    return alert
                except Exception as e:
                    raise RuntimeError("Snow: ERROR - %s" % e)
                LOG.info('Snow: %s - %s', response.status_code, response.text)
        else:
            if alert.severity in ["normal","ok"]:
                payload = {
	                "ServiceNow": {
		                "number": "%s" % ServiceNowID,
		                "company": "%s" % COMPANY,
		                "state": "Closed"
                    }
                } 
                try:
                    LOG.info("Closing alert %s and SNow ID %s"  % (alert.id, ServiceNowID))
                    response = requests.request("POST", SNOW_WEBHOOK_URL, json=payload, headers=headers)
                    alert.attributes['ServiceNowStatus'] = "closed"
                    return alert
                except Exception as e:
                    raise RuntimeError("Snow: ERROR - %s" % e)
            else:
                LOG.info('Alert post receive else %s: %s' % (alert.id, alert.get_body(history=False)))
                return alert

    def status_change(self, alert, status, text):
        printedAlert = str(alert)
        LOG.info("alert iniziale status change%s" % printedAlert)
        try:
            ServiceNowID = alert.attributes['ServiceNow']
            payload = {
                "ServiceNow": {
                    "number": "%s" % ServiceNowID,
                    "company": "%s" % COMPANY,
                    "state": "Closed"
                }
            } 
            LOG.info("Closing alert %s with status %s and SNow ID %s"  % (alert.id, status, ServiceNowID))
            response = requests.request("POST", SNOW_WEBHOOK_URL, json=payload, headers=headers)
            LOG.info('Snow response: %s - %s', response.status_code, response.text)
            alert.attributes['ServiceNowStatus'] = "closed"
            return alert
        except Exception as e:
            LOG.info("non riesco a chiudere  alert %s" % (alert.id))
            pass
        return alert
