
from setuptools import setup, find_packages

version = '5.0.0'

setup(
    name="alerta-snow",
    version=version,
    description='Alerta plugin for Rocket.Chat',
    url='https://jackfrost82@bitbucket.org/jackfrost82/alerta.git',
    license='MIT',
    author='Nick Satterly',
    author_email='nick.satterly@gmail.com',
    packages=find_packages(),
    py_modules=['alerta_snow'],
    install_requires=[
        'requests'
    ],
    include_package_data=True,
    zip_safe=True,
    entry_points={
        'alerta.plugins': [
            'snow = alerta_snow:PostMessage'
        ]
    }
)
